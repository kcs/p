#!/bin/sh

OD="od.k`hostname | cut -c 1-2`.kcs"
CURRENTOD="$(/usr/bin/dscl localhost -list /LDAPv3)"

while read -r line; do
	if [ "${line}" != "${OD}" ]; then
		echo "Removing ${line}..."
		/usr/bin/dscl -q localhost -delete /Search CSPSearchPath "/LDAPv3/${line}"
		/usr/sbin/dsconfigldap -r "${line}"
	fi
done <<< "$(/usr/bin/dscl localhost -list /LDAPv3)"


CURRENTOD=`/usr/bin/dscl localhost -list /LDAPv3`

if [ "${CURRENTOD}" != "${OD}" ]; then
	echo "Adding ${OD}..."
	yes | /usr/sbin/dsconfigldap -a "${OD}"
	/usr/bin/dscl /Search -create / SearchPolicy CSPSearchPath
	/usr/bin/dscl -q localhost -merge /Search CSPSearchPath "/LDAPv3/${OD}"
fi

if [[ -z $(/usr/bin/dscl -q localhost -read /Search CSPSearchPath | grep ${OD}) ]]; then
	echo "Adding /LDAPv3/${OD} to CSPSearchPath..."
	/usr/bin/dscl -q localhost -merge /Search CSPSearchPath "/LDAPv3/${OD}"
fi

if [[ -z $(/usr/bin/dscl -q localhost -read /Search SearchPath | grep ${OD}) ]]; then
	echo "Adding /LDAPv3/${OD} SearchPath..."
	/usr/bin/dscl -q localhost -delete /Search CSPSearchPath "/LDAPv3/${OD}"
	/usr/bin/dscl /Search -create / SearchPolicy CSPSearchPath
	/usr/bin/dscl -q localhost -append /Search CSPSearchPath "/LDAPv3/${OD}"
fi
