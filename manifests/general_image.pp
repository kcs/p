# /etc/puppet/manifests/classes/general_image.pp

class general_image {

# Turn off Gatekeeper
exec { "sudo spctl --master-disable":
  user  => "root",
  unless => "spctl --status | grep disabled",
  }  

#Turn off Autologin
#	exec { "defaults write /Library/Preferences/com.apple.loginwindow autoLoginUser \"\"":
#		onlyif	=> ["test `defaults read /Library/Preferences/com.apple.loginwindow autoLoginUser`"]
#	}

#Set computer info 4 to puppet version
	exec { "sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -configure -computerinfo -set4 -4 \"From Git `puppet -V`\"":
		unless => "defaults read /Library/Preferences/com.apple.RemoteDesktop Text4 |grep \"From Git `puppet -V`\" ",
	}

# Set the machine to ask for username and password
exec{ "loginusername":
  command  => "defaults write /Library/Preferences/com.apple.loginwindow SHOWFULLNAME -bool true",
  user  => "root",
  unless  => ["test `defaults read /Library/Preferences/com.apple.loginwindow SHOWFULLNAME` = \"1\" "], 
}

# Turn on multiple user logins
exec { "multipleusers":
  command => "defaults write /Library/Preferences/.GlobalPreferences.plist MultipleSessionEnabled -bool true",
  user    => "root", 
}

#Set computer info 2 to the time puppet last ran
	exec { "sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -configure -computerinfo -set2 -2 \"`/usr/bin/dscl localhost -list \"/Active Directory\"`\"":
	}


#Set computer info 3 to the time puppet last ran
	exec { "sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -configure -computerinfo -set3 -3 \"`date \"+%y%m%d %H:%M:%S\"`\"":
	}

#Set the correct timezone
	exec { "systemsetup -settimezone America/New_York":
		onlyif	=> ["test `systemsetup -gettimezone | grep \"Time Zone\"| cut -c 12-` != \"America/New_York\""],
	}
	exec { "systemsetup -setusingnetworktime on":
		onlyif	=> ["test `systemsetup -getusingnetworktime | grep \"Network Time\" | cut -c 15-16` != \"On\""],
	}
	exec { "systemsetup -setnetworktimeserver time.apple.com":
		onlyif	=> ["test `systemsetup -getnetworktimeserver | grep \"Network Time\" | cut -c 22-` != \"time.apple.com\""],
	}

#Turn on sshd
	exec { "systemsetup -setremotelogin on":
		onlyif	=> ["test `systemsetup -getremotelogin | grep \"Remote\"| cut -c 15-16` != \"On\""],
	}

	exec { "/System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -access -on -users kadmin -privs -all -restart -agent-menu": 
		onlyif	=> ["test !`ps ax | grep -v grep | grep ARD`"],
	}
# Set LoginWindow background
#        property_list_key { 'LoginWindow-background':
#                ensure     => present,
#                path       => '/Library/Preferences/com.apple.loginwindow.plist',
#                key        => 'DesktopPicture',
#                value      => '/Library/Desktop Pictures/Ripples Blue.jpg',
#                value_type => 'string',
#        }


# Set energy saver
	exec { "pmset -a sleep 0":
		# There is a space and a tab between the brackets
		onlyif => ["test `pmset -g | sed -e 's/^[ 	]*//' | sed -E 's/[ 	]+/,/g' | grep ^sleep | cut -d ',' -f 2` != '0'"]
	}
	exec { "pmset -a displaysleep 45":
		onlyif => ["test `pmset -g | sed -e 's/^[ 	]*//' | sed -E 's/[ 	]+/,/g' | grep ^displaysleep | cut -d ',' -f 2` != '45'"]
	}

	exec { "pmset -a disksleep 60":
		onlyif => ["test `pmset -g | sed -e 's/^[ 	]*//' | sed -E 's/[ 	]+/,/g' | grep ^disksleep | cut -d ',' -f 2` != '60'"]
	}

	if  ( $sp_local_host_name =~ /11954/ ) or ( $sp_machine_name =~ /MacBook/ ) or ( $sp_local_host_name =~/12150/ ) or ( $sp_local_host_name == "OL-Chewy-11286" ) or ( $ipaddress =~ /10.70.5.3/ ) or ( $sp_local_host_name =~/11907/ ) or ( $sp_local_host_name =~/13388/ ){
            exec { "pmset repeat cancel" :
            onlyif => ["test `pmset -g sched | wc -l  | cut -d \" \" -f8` != 0"],
            }
    } else {
      exec { "pmset repeat cancel;pmset repeat shutdown MTWRFSU 23:00:00 wakeorpoweron MTWRFSU 20:30:00" :
      #exec { "pmset repeat cancel;pmset repeat wakeorpoweron MTWRF 6:00:00" :
      # 	exec { "pmset repeat cancel" :
		    unless => ["pmset -g sched | grep 'wakepoweron at 8:30PM'",
			       "pmset -g sched | grep 'shutdown at 11:00PM'"
			      ],
		    }
	}


# Turn off Apple Software Update
	exec { "softwareupdate --schedule off":
		onlyif => ["/usr/sbin/softwareupdate --schedule | grep on"],
	}

# Set puppet to run every 30 minutes
file { '/Library/LaunchDaemons/org.kentoncityschools.puppetrun.plist' :
  ensure  => present,
  owner   => 'root',
  mode    => '0644',
  source  => '/etc/puppetlabs/puppet/files/macOS/org.kentoncityschools.puppetrun.plist',
  alias   => 'puppetrun',
}

service { "org.kentoncityschools.puppetrun":
    enable          =>      true,
    ensure          =>      running,
    subscribe       =>      File["puppetrun"],
    require         =>      File["puppetrun"],
}

# Turn off the default puppet daemon
service { "puppet" :
  ensure    => stopped,
  enable    => false,
}


#Licenses and other preferences
#	file { "/Library/Application\ Support/plasq/Licenses":
#		ensure		=> directory,
#		recurse		=> true,
#	}

#	file { "/Library/Application\ Support/plasq/Licenses/Comic\ Life.plasqlicense":
#		ensure		=> file,
#		owner		=> "kadmin",
#		group		=> "wheel",
#		mode		=> 755,
#		source		=> "puppet:///files/licenses/Comic%20Life.plasqlicense",
#	}

# Preferences
#file { "/Library/Preferences":
#	ensure	=>	directory,
#	recurse	=>	remote,
#  owner	=>	"root",
#  group	=>	"wheel",
#  #mode	=>	644,
#	source	=>	"puppet:///files/Preferences",
#}
#
#file {"/Library/Application Support/Macromedia":
#    ensure  => directory,
#}
#
#file {"/Library/Application Support/Macromedia/mms.cfg":
#    ensure  => file,
#    source  => "puppet:///files/Preferences/mms.cfg",
#    require => File['/Library/Application Support/Macromedia'],
#}
#

# Fonts
#file { "/Library/Fonts":
#    ensure      => directory,
#    recurse     => true,
#    owner       => "root",
#    group       => "wheel",
#    mode        => "644",
#    source      =>  "/etc/puppetlabs/puppet/files/macOS/Fonts",
#}

#Login Hooks	
file { "/etc/hooks":
  ensure  => directory,
  recurse => true,
  owner   => "root",
  group   => "wheel",
  mode    => "755",
  purge   => true,
  source  => "/etc/puppetlabs/puppet/files/macOS/hooks",
}

file { "/etc/login.hook":
        ensure          => file,
        owner           => "root",
        group           => "wheel",
        mode            => "755",
        source          => "/etc/puppetlabs/puppet/files/macOS/hooks/login.hook",
}
file { "/etc/logout.hook":
        ensure          => file,
        owner           => "root",
        group           => "wheel",
        mode            => "755",
        source          => "/etc/puppetlabs/puppet/files/macOS/hooks/logout.hook",
}

exec {"/usr/bin/defaults write /Library/Preferences/com.apple.loginwindow LoginHook '/etc/login.hook'":
  unless => "test `/usr/bin/defaults read /Library/Preferences/com.apple.loginwindow LoginHook` = '/etc/login.hook'",
}
exec {"/usr/bin/defaults write /Library/Preferences/com.apple.loginwindow LogoutHook '/etc/logout.hook'":
  unless => "test `/usr/bin/defaults read /Library/Preferences/com.apple.loginwindow LogoutHook` = '/etc/logout.hook'",
}	


#Install default packages
#	case $macosx_productversion_major {
#		10.5: { 
#			include leopard
#		       }			
#		10.6: { 
#			include snowleopard
#	               }
#		10.7: {
#			include lion
#		}
#		10.8: {
#			include mountainlion
#		}
#	}
}
