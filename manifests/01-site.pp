Exec {path => "/opt/puppetlabs/bin:/usr/bin:/usr/sbin:/bin:/sbin"}

#Global varials, start with kcs_

$kcs_files = "https://www.kentoncityschools.org/media/puppet"

node default {
  file { "/Users/kadmin/test" :
    ensure  => present,
    owner   => "kadmin",
    group   => "staff",
    mode    => "0644",
    source  => "/etc/puppetlabs/puppet/files/test",
  }

}

# Teacher Machines
node /^(ec|es|hc|hs|ms|ol|nw|wv).*-d-/ {
  include general_image
  include munki
  #include odbind
  include printers
}


# Teacher Machines
node /^(ec|es|hc|hs|ms|ol|nw|wv).*-td-/ {

  file { "/Users/kadmin/test" :
    ensure  => present,
    owner   => "kadmin",
    group   => "staff",
    mode    => "0644",
    source  => "$::kcs_files/files/test",
  }

  include general_image
  include munki
  #include odbind
  include printers
}

# Laptops
node /^(ec|es|hc|hs|ms|ol|nw|wv).*-l-/ {
  include general_image
  include ol_color_copier_2016
  include munki
  #include odbind
  include printers
}

# Clock computers
node /.*bell.*/ {
  include bell
}
