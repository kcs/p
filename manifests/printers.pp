class kcs_copier {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/kcs-copier",
        description     => "(KCS) Copier",
   }


    if $operatingsystem == 'Darwin' {
        printer { "kcs-copier": ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTAC659.gz", } 
        exec { "lpadmin -p kcs-copier -o KMDuplex=Single":
          require => Printer['kcs-copier'],
        }
    } else {
        printer { "kcs-copier": ppd => "/usr/share/cups/model/KMbeuC554ux.ppd", }
    }
} #Class kcs_copier

class es_printers {
    include es_confa_printer
    include es_conf_2019
    include es_butterman_printer
    include es_best_printer
    include es_wise_printer
    
    include es_office_copier_2019
    include es_a_copier_2019
    include es_b_copier_2019
    include es_c_copier_2019
    include es_d_copier_2019

    include es_a_copier
    include es_b_copier_2016
    include es_office_copier_2016
    include es_c_copier_2016
    include es_d_copier_2016
}

class ms_printers {
    include ms_lmc_copier
    include ms_lmc_copier_2019
    include ms_office_copier_2019
    include ms_kola_printer
    include ms_backhall_printer
}

class hs_printers {
    include hs_middlehall_copier
    include hs_office_copier_2016

    include hs_office_copier_2019
    include hs_middlehall_copier_2019
    include hs_upstairs_copier_2019
}

class ol_printers {
  include ol_color_copier_2016
  include ol_color_copier_2019
	include ol_emis_copier 
}

class printers {

    if $operatingsystem == 'Darwin' {

          file { "/Library/Printers/PPDs/Contents/Resources/konica751-fs524.ppd":                
                ensure          => file,
                owner           => "root",
                group           => "wheel",                
                mode            => "755",
                source         => "/etc/puppetlabs/puppet/files/macOS/PPDs/konica751-fs524.ppd",
            }
           file { "/Library/Printers/PPDs/Contents/Resources/c360.ppd":                
                ensure          => file,
                owner           => "root",
                group           => "wheel",                
                mode            => "755",
                source         => "/etc/puppetlabs/puppet/files/macOS/PPDs/c360.ppd",
            }

            file { "/Library/Printers/PPDs/Contents/Resources/c364e.ppd":                
                ensure          => file,
                alias           => "c364e",
                owner           => "root",
                group           => "wheel",                
                mode            => "755",
                source         => "/etc/puppetlabs/puppet/files/macOS/PPDs/c364e.ppd",
            }

           file { "/Library/Printers/PPDs/Contents/Resources/konica654e-fs534.ppd":                
                ensure          => file,
                alias           => "konica654e",
                owner           => "root",
                group           => "wheel",                
                mode            => "755",
                source         => "/etc/puppetlabs/puppet/files/macOS/PPDs/konica654e-fs534.ppd",
            }

        } else {
            package { 'cups':
                ensure  => installed,
                require => Exec['aptupdate'],
            }

            service { 'cups':
                ensure  => running,
                enable  => true,
                require => Package['cups'],
            }

            file { '/usr/share/cups/model':
                    alias           => "PPDs",
                    ensure          => directory,
                    owner           => "root",
                    group           => "root",
                    mode            => "644",
                    source          => "/etc/puppetlabs/puppet/files/macOS/PPDs",
                    recurse         => true,
                    require => Package['cups'],
            }

            file { '/usr/lib/cups/filter':
                    alias           => "filters",
                    ensure          => directory,
                    owner           => "root",
                    group           => "root",
                    mode            => "755",
                    source          => "/etc/puppetlabs/puppet/files/macOS/filters",
                    recurse         => true,
            }


            file { "/var/lib/KOC364UX.ppd":
                alias   => "copierKOC364UXppd",
                owner           => "root",
                group           => "root",
                mode            => "664",
                source  => "/etc/puppetlabs/puppet/files/macOS/PPDs/KOC364UX.ppd",
            }

           file { "/var/lib/KO750opn.ppd":
                alias   => "copier750ppd",
                owner           => "root",
                group           => "root",
                mode            => "664",
                source  => "/etc/puppetlabs/puppet/files/macOS/PPDs/KO750opn.ppd",
            }

           file { "/var/lib/KOC360opn.ppd":
                alias   => "copierC360ppd",
                owner           => "root",
                group           => "root",
                mode            => "664",
                source  => "/etc/puppetlabs/puppet/files/macOS/PPDs/KOC360opn.ppd",
            }
            file { "/var/lib/Brother8890.ppd":
                alias   => "Brother8890",
                owner   => "root",
                group   => "root",
                mode    => "664",
                source  => "/etc/puppetlabs/puppet/files/macOS/PPDs/Brother8890.ppd",
            }
            file { "/var/lib/KyoceraCS255ci.ppd":
                alias   => "KyoceraCS255ci",
                owner   => "root",
                group   => "root",
                mode    => "664",
                source  => "/etc/puppetlabs/puppet/files/macOS/PPDs/KyoceraCS255ci.ppd",
            }
    }

case $fqdn {

  /^es-/ : { 
        include es_printers
        include kcs_copier
        #include nw-printers
		    }

	/^ol-/ : { 
    include ol_printers
        include kcs_copier
           }

	/^hs-/ : { 
  include hs_printers
        include kcs_copier
  #include hs-middlehall-copier
           }

	/^ms-/ : { 
    include ms_printers 
        include kcs_copier
          }

	/^nw-/ : { 
            include ol_printers
	    include es_printers
        include kcs_copier
            } 		
 
	} # case

  #Bame Macbook
  if ($sp_local_host_name =~ /15749/) {
    include es_bame_printer
  } 

  if ($sp_local_host_name =~ /15625/ ) { include ms_printers } # FreyM laptop 
  if ($sp_local_host_name =~ /15646/ ) { include ol_printers } # PhillipsJ
  if ($sp_local_host_name =~ /15747/ ) { include ol_printers } 
  if ($sp_local_host_name =~ /15761/ ) { include ol_printers } 
  if ($sp_local_host_name =~ /15644/ ) { include hs_printers } 


  if ($sp_local_host_name =~ /15506/) or ($sp_local_host_name =~ /15690/) { 
  	include es_printers 
  	include hs_printers
    include ms_printers
  } # Heather Hoppe & Anne Winings
  
  
  #if ($sp_local_host_name == "es-sheldone-l-12431" ) { include wv-printers } 
  #
  #if ($sp_local_host_name == "es-cribley-l-12457" ) { include ol-printers } 
  #
  #if $sp_local_host_name =~ /12399/  { include nw-printers } # Rick Collins
  #
  #if $sp_local_host_name =~ /12400/ { include nw-printers } # McEntee
  #
  ##Mendez
  #if $sp_local_host_name =~ /15708/ { 
  #	include ol-color-copier-2016
  #  include es-office-copier-2016
  #} 
  #
  #if $sp_local_host_name =~ /12645/ { 
  #	include nw-printers 
  #	include ol-color-copier-2016
  #} # Wise
  #
  ##Thrush
  #if $sp_local_host_name =~ /11246/ { 
  #    include hs-205-color-printer
  #    include hs-lmc-color-printer
  #}
  #
  ##Gilbert
  #if $sp_local_host_name =~ /15614/ {
  #  include hs-printers
  #} 
  #
  ##DavisM
  #if $sp_local_host_name =~ /15620/ {
  #  include es-printers
  #} 
  #
  #
  #
  ##Fox
  #if $sp_local_host_name =~ /15631/ {
  #  include ms-printers
  #  include hs-printers
  #} 
  #
  ##Georgeson
  #if $sp_local_host_name =~ /15547/ {
  #  include ms-printers
  #  include hs-printers
  #} 
  #
  ##Rogers
  #if $sp_local_host_name =~ /15645/  { 
  #    include hs-middlehall-copier
  #} 
  #
  ##Snyder
  #if $sp_local_host_name =~ /15608/  { 
  #    include ms-printers
  #} 
  #
  ##Kohl
  #if $sp_local_host_name =~ /15641/  { 
  #    include hs-middlehall-copier
  #} 
  #
  #if $sp_local_host_name =~ /12435/  { 
  #    include nw-printers 
  #    include hs-printers
  #} # Andrea Schneider
  #
  #if $sp_local_host_name =~ /12826/ {
  #    include ec-printers
  #    include ol-printers
  #} # Butterman
  #
  #if $sp_local_host_name =~ /15600/ {
  #    include ms-printers
  #} # Hoppe, Bo
  #
  #if $sp_local_host_name =~ /12150/ {
  #  include ol-color-copier-2016
  #  include es-b-copier-2016
  #  include hs-office-copier-2016
  #  include es-office-copier-2016
  #  include es-c-copier-2016
  #  include es-d-copier-2016
  #} # TC Hackintosh

} # class printers

class ol_color_copier_2016 {
   Printer {
       ensure  => absent,
       shared  => false,
       uri     => "ipp://papercut-ps.thekcs.net/printers/ol-color-copier-2016",
       description     => "(OL) Color Copier 2016 on Papercut",
  }
} #class ol_color_copier_2016

class ol_color_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/ol-color-copier-2019",
        description     => "(OL) Color Copier 2019",
   }
        if $operatingsystem == 'Darwin' {
            printer { "ol-color-copier-2019": ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTAC659.gz", } 
            exec { "lpadmin -p ol-color-copier-2019 -o KMDuplex=Single":
                             require => Printer['ol-color-copier-2019'],
            }     
          
          } else {
            printer { "ol-color-copier-2019": ppd => "/usr/share/cups/model/KMbeuC554ux.ppd", }
        }
} #class ol_color_copier_2019

class ol_emis_copier {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/ol-emis-copier",
        description     => "(OL) EMIS Copier on Papercut",
   }

			if $operatingsystem == 'Darwin' {
					printer { "ol-emis-copier":
							ppd => "/Library/Printers/PPDs/Contents/Resources/konica751-fs524.ppd",
							require => File["/Library/Printers/PPDs/Contents/Resources/konica751-fs524.ppd"],
					}
			} else {
					printer { "ol-emis-copier":
							ppd     => "/var/lib/KO750opn.ppd",
							require => [ Package['cups'], File['copier750ppd'] ],
					}
			}
} #class ol_emis_copier

class es_a_copier {
    Printer {
        ensure  => absent,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-a-copier",
        description     => "(ES) A Copier on Papercut",
   }

}

class es_b_copier_2016 {
    Printer {
        ensure  => absent,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-b-copier-2016",
        description     => "(ES) B Copier 2016 on Papercut  ",
   }

}

class es_c_copier_2016 {
    Printer {
        ensure  => absent,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-c-copier-2016",
        description     => "(ES) C Copier 2016 on Papercut",
   }
}

class es_d_copier_2016 {
    Printer {
        ensure  => absent,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-d-copier-2016",
        description     => "(ES) D Copier 2016 on Papercut",
   }
}

class es_conf_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-conf-2019",
        #        uri     => "socket://hp-13285.kes.kcs:9100/",
        description     => "(ES) Conf Printer 2019",
   }

        if $operatingsystem == 'Darwin' {
            printer { "es-conf-2019": ppd => "/Library/Printers/PPDs/Contents/Resources/HP LaserJet 500 color M551.gz", }
        } else {
            printer { "es-conf-2019": model   => "gutenprint.5.2://hp-lj_4100/expert", }
        }
}



class es_confa_printer {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-confa-printer",
        description => "(ES) Conf A Printer on Papercut",
    }

    if $fqdn =~ /^[a-z].*-ps/ {
        printer { "es-confa-printer":
            ensure  => present,
            shared  => true,
            uri     => "socket://confa-printer.kes.kcs:9100/",
            description     => "(ES) Conf A Printer",
            model   => "gutenprint.5.2://hp-lj_4050/expert",
        }
                exec { "lpadmin -p es-confa-printer -o media=letter":
                    require => Printer['es-confa-printer'],
                    unless  => "grep -q '^*DefaultPageSize: Letter' /etc/cups/ppd/es-confa-printer.ppd",
                }

    } else {
        if $operatingsystem == 'Darwin' {
            printer { "es-confa-printer": ppd     => "/Library/Printers/PPDs/Contents/Resources/HP LaserJet 4050 Series.gz", }
        } else {
            printer { "es-confa-printer": model   => "gutenprint.5.2://hp-lj_4050/expert",}
        }
    }
}

class es_butterman_printer {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-butterman-printer",
        #        uri     => "socket://hp-13285.kes.kcs:9100/",
        description     => "(ES) Butterman Office Printer on Papercut",
   }

    if $fqdn =~ /^[a-z].*-ps/ {
        printer { "es-butterman-printer":
            ensure  => present,
            shared  => true,
            uri     => "socket://butterman-printer.kes.kcs:9100/",
            description     => "(ES) Butterman Office Printer",
            model   => "gutenprint.5.2://hp-lj_4100/expert",
        }
                exec { "lpadmin -p es-butterman-printer -o media=letter":
            require => Printer['es-butterman-printer'],
            unless  => "grep -q '^*DefaultPageSize: Letter' /etc/cups/ppd/es-butterman-printer.ppd",
        }
    } else {
        if $operatingsystem == 'Darwin' {
            printer { "es-butterman-printer": ppd => "/Library/Printers/PPDs/Contents/Resources/HP LaserJet 500 color M551.gz", }
        } else {
            printer { "es-butterman-printer": model   => "gutenprint.5.2://hp-lj_4100/expert", }
        }
    }
}

class es_wise_printer {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-wise-printer",
        #        uri     => "socket://hp-13285.kes.kcs:9100/",
        description     => "(ES) Wise Office Printer on Papercut",
   }

    if $fqdn =~ /^[a-z].*-ps/ {
        printer { "es-wise-printer":
            ensure  => present,
            shared  => true,
            uri     => "socket://butterman-printer.kes.kcs:9100/",
            description     => "(ES) Butterman Office Printer",
            model   => "gutenprint.5.2://hp-lj_4100/expert",
        }
                exec { "lpadmin -p es-butterman-printer -o media=letter":
            require => Printer['es-butterman-printer'],
            unless  => "grep -q '^*DefaultPageSize: Letter' /etc/cups/ppd/es-butterman-printer.ppd",
        }
    } else {
        if $operatingsystem == 'Darwin' {
            printer { "es-wise-printer": ppd => "/Library/Printers/PPDs/Contents/Resources/HP LaserJet 500 color M551.gz", }
        } else {
            printer { "es-wise-printer": model   => "gutenprint.5.2://hp-lj_4100/expert", }
        }
    }
}

class es_bame_printer {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-bame-printer",
        #        uri     => "socket://hp-13285.kes.kcs:9100/",
        description     => "(ES) Bame Office Printer on Papercut",
   }

    if $fqdn =~ /^[a-z].*-ps/ {
        printer { "es-wise-printer":
            ensure  => present,
            shared  => true,
            uri     => "socket://butterman-printer.kes.kcs:9100/",
            description     => "(ES) Butterman Office Printer",
            model   => "gutenprint.5.2://hp-lj_4100/expert",
        }
                exec { "lpadmin -p es-butterman-printer -o media=letter":
            require => Printer['es-butterman-printer'],
            unless  => "grep -q '^*DefaultPageSize: Letter' /etc/cups/ppd/es-butterman-printer.ppd",
        }
    } else {
        if $operatingsystem == 'Darwin' {
            printer { "es-bame-printer": ppd => "/Library/Printers/PPDs/Contents/Resources/HP LaserJet 500 color M551.gz", }
        } else {
            printer { "es-bame-printer": model   => "gutenprint.5.2://hp-lj_4100/expert", }
        }
    }
}

class es_best_printer {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/best-printer",
        #        uri     => "socket://hp-13285.kes.kcs:9100/",
        description     => "(ES) Best Office Printer on Papercut",
   }

    if $fqdn =~ /^[a-z].*-ps/ {
        printer { "es-best-printer":
            ensure  => present,
            shared  => true,
            uri     => "socket://butterman-printer.kes.kcs:9100/",
            description     => "(ES) Butterman Office Printer",
            model   => "gutenprint.5.2://hp-lj_4100/expert",
        }
                exec { "lpadmin -p es-butterman-printer -o media=letter":
            require => Printer['es-butterman-printer'],
            unless  => "grep -q '^*DefaultPageSize: Letter' /etc/cups/ppd/es-butterman-printer.ppd",
        }
    } else {
        if $operatingsystem == 'Darwin' {
            printer { "es-best-printer": ppd => "/Library/Printers/PPDs/Contents/Resources/HP LaserJet 500 color M551.gz", }
        } else {
            printer { "es-best-printer": model   => "gutenprint.5.2://hp-lj_4100/expert", }
        }
    }
}

class es_office_copier_2016 {
    Printer {
        ensure  => absent,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-office-copier-2016",
        description     => "(ES) Office Copier 2016 on Papercut",
   }

} # Class es_office_copier_2016

class es_office_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-office-copier-2019",
        description     => "(ES) Office Copier 2019",
   }


    if $fqdn =~ /ps/ {
        printer { "es-office-copier-2019":
            shared  => true,
            uri     => "socket://office-copier.kes.kcs:9100/",
            description     => "(ES) Office Copier 2019",
            ppd     => "/usr/share/cups/model/KMbeuC658ux.ppd",
            require => [ Package['cups'], File['PPDs'], File['filters'] ],
        }
            exec { "lpadmin -p es-office-copier-2019 -o media=letter":
              require => Printer['es-office-copier-2019'],
              unless  => "grep -q '^*DefaultPageSize: Letter' /etc/cups/ppd/es-office-copier-2016.ppd",
            }
                                                
    } else {
        if $operatingsystem == 'Darwin' {
            printer { "es-office-copier-2019": ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTAC659.gz", } 
            exec { "lpadmin -p es-office-copier-2019 -o KMDuplex=Single":
                 require => Printer['es-office-copier-2019'],
               }  
        } else {
            printer { "es-office-copier-2019": ppd => "/usr/share/cups/model/KMbeuC658ux.ppd", }
        }
    }
} #Class es_office-copier-2019

class es_a_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-a-copier-2019",
        description     => "(ES) A Copier 2019",
   }
    if $operatingsystem == 'Darwin' {
        printer { "es-a-copier-2019":
            ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTA958.gz",
        }
            exec { "lpadmin -p es-a-copier-2019 -o KMDuplex=Single":}
    } else {
        printer { "es-a-copier":
            ppd     => "/var/lib/KO750opn.ppd",
            require => [ Package['cups'], File['copier750ppd'] ],
        }
    }
} #class es_a_copier_2019

class es_b_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-b-copier-2019",
        description     => "(ES) B Copier 2019",
   }

        if $operatingsystem == 'Darwin' {
            printer { "es-b-copier-2019":
                ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTA958.gz",
                }
           exec { "lpadmin -p es-b-copier-2019 -o KMDuplex=Single":
                            require => Printer['es-b-copier-2019'],
           }
        } else {
            printer { "es-b-copier-2019":
                ppd     => "/usr/share/cups/model/KMbeu958ux.ppd",
                require => [ Package['cups'], File['/usr/share/cups/model'] ],
            }
        }
} #Class es_b_copier_2019

class es_c_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-c-copier-2019",
        description     => "(ES) C Copier 2019",
   }
        if $operatingsystem == 'Darwin' {
            printer { "es-c-copier-2019":
                ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTA958.gz",
                }
           exec { "lpadmin -p es-c-copier-2019 -o KMDuplex=Single":
                            require => Printer['es-c-copier-2019'],
                            }
        } else {
            printer { "es-c-copier-2019":
                ppd     => "/usr/share/cups/model/KMbeu958ux.ppd",
                require => [ Package['cups'], File['/usr/share/cups/model'] ],
            }
        }
} #Class es_c_copier_2019

class es_d_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-d-copier-2019",
        description     => "(ES) D Copier 2019",
   }
        if $operatingsystem == 'Darwin' {
            printer { "es-d-copier-2019":
                ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTA958.gz",
                }
           exec { "lpadmin -p es-d-copier-2019 -o KMDuplex=Single":
                            require => Printer['es-d-copier-2019'],
                            }
        } else {
            printer { "es-d-copier-2019":
                ppd     => "/usr/share/cups/model/KMbeu958ux.ppd",
                require => [ Package['cups'], File['/usr/share/cups/model'] ],
            }
        }
} #Class es_d_copier_2019 

#END ES Printers

# MS Printers
class ms_backhall_printer {
    Printer {
        ensure  => absent,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/es-a-copier",
        description     => "(MS) Backhall Printer on Papercut",
   }

}


class ms_lmc_copier {
    Printer {
        ensure  => absent,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/ms-lmc-copier",
        description     => "(MS) LMC Copier on Papercut",
   }
}

class ms_lmc_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/ms-lmc-copier-2019",
        description     => "(MS) LMC Copier 2019",
   }

        if $operatingsystem == 'Darwin' {
            printer { "ms-lmc-copier-2019":
                ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTA958.gz",
                }
           exec { "lpadmin -p ms-lmc-copier-2019 -o KMDuplex=Single":
             #               require => Printer['ms_lmc_copier_2019'],
           }
        } else {
            printer { "ms-lmc-copier-2019":
                ppd     => "/usr/share/cups/model/KMbeu958ux.ppd",
                require => [ Package['cups'], File['/usr/share/cups/model'] ],
            }
        }
} #Class es_b_copier_2019


class ms_office_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/ms-office-copier-2019",
        description     => "(MS) Office Copier 2019",
   }
        if $operatingsystem == 'Darwin' {
            printer { "ms-office-copier-2019":
                ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTAC659.gz",
            }
           exec { "lpadmin -p ms-office-copier-2019 -o KMDuplex=Single": }
        } else {
            printer { "ms-fronthall-copier":
                ppd     => "/var/lib/KO750opn.ppd",
                require => [ Package['cups'], File['copier750ppd'] ],
            }
        }
} #Class ms_office_copier

class ms_kola_printer {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/ms-kola-printer",
        description     => "(MS) KOLA Printer on Papercut",
   }

        if $operatingsystem == 'Darwin' {
            printer { "ms-kola-printer": ppd => "/Library/Printers/PPDs/Contents/Resources/HP LaserJet 4050 Series.gz", }
        } else {
            printer { "ms-kola-printer": model   => "gutenprint.5.2://hp-lj_4050/expert", }
        }
} # Class ms_kola_printer

# END MS Printers

# HS Printers
class hs_office_copier_2016 {
    Printer {
        ensure  => absent,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/hs-office-copier-2016",
        description     => "(HS) Office Copier 2016 on Papercut",
   }

} # hs_office_copier_2016

class hs_middlehall_copier {
    Printer {
        ensure  => absent,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/hs-middlehall-copier",
        description     => "(HS) Middle Hall Copier on Papercut",
   }

} # hs_middlehall_copier


class hs_middlehall_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/hs-middlehall-copier-2019",
        description     => "(HS) Middle Hall Copier 2019",
   }
      if $operatingsystem == 'Darwin' {
          printer { "hs-middlehall-copier-2019":
            ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTA808.gz",
              require => File['konica654e'],
          }
          exec { "lpadmin -p hs-middlehall-copier-2019 -o KMDuplex=Single": }
      }

} # hs_middlehall_copier_2019


class hs_office_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/hs-office-copier-2019",
        description     => "(HS) Office Copier 2019",
   }


    if $fqdn =~ /ps/ {
        printer { "hs-office-copier-2019":
            shared  => true,
            uri     => "socket://office-copier.khs.kcs:9100/",
            description     => "(HS) Office Copier 2019",
            ppd     => "/usr/share/cups/model/KMbeuC658ux.ppd",
            require => [ Package['cups'], File['PPDs'], File['filters'] ],
        }

        exec { "lpadmin -p hs-office-copier-2019 -o media=letter":
          require => Printer['hs-office-copier-2019'],
          unless  => "grep -q '^*DefaultPageSize: Letter' /etc/cups/ppd/hs-office-copier-2019.ppd",
        }          

    } else {
        if $operatingsystem == 'Darwin' {
            printer { "hs-office-copier-2019": ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTAC659.gz", } 
            exec { "lpadmin -p hs-office-copier-2019 -o KMDuplex=Single":
              require => Printer['hs-office-copier-2019'],
            }
        } else {
            printer { "hs-office-copier-2019": ppd => "/usr/share/cups/model/KMbeuC554ux.ppd", }
        }
    }
} #Class hs_office_copier_2019

class hs_upstairs_copier_2019 {
    Printer {
        ensure  => present,
        shared  => false,
        uri     => "ipp://papercut-ps.thekcs.net/printers/hs-upstairs-copier-2019",
        description     => "(HS) Upstairs Copier 2019",
   }

    if $fqdn =~ /^[a-z].*-ps/ {
        printer { "hs-upstairs-copier-2019":
            ensure  => present,
            shared  => true,
            uri     => "socket://upstairs-copier.khs.kcs:9100/",
            description     => "(HS) Upstairs Copier 2019",
            ppd     => "/var/lib/KO750opn.ppd",
            require => [ Package['cups'], File['copier750ppd'] ],
        }
    } else {
        if $operatingsystem == 'Darwin' {
            printer { "hs-upstairs-copier-2019":
              ppd => "/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTA808.gz",
                require => File['konica654e'],
            }
            exec { "lpadmin -p hs-upstairs-copier-2019 -o KMDuplex=Single": }
        } else {
            printer { "hs-upstairs-copier-2019":
                ppd     => "/var/lib/KO750opn.ppd",
                require => [ Package['cups'], File['copier750ppd'] ],
            }
        }
    }
} #hs_upstairs_copier_2019

