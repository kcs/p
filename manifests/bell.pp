class bell {
        file { "/Library/Scripts/ring.sh":
        ensure          => file,
        owner           => "root",
        group           => "admin",
        mode            => 755,
        source          => "$::kcs_files/files/bell/ring.sh",
        }                                                                          

        file { "/Library/Scripts/WeWish.aiff":
        ensure          => file,
        owner           => "root",
        group           => "admin",
        mode            => 755,
        source          => "$::kcs_files/files/bell/WeWish.aiff",
        }                                                                          

        file { "/Library/Scripts/bell.m4a":
        ensure          => file,
        owner           => "root",
        group           => "admin",
        mode            => 755,
        source          => "$::kcs_files/files/bell/AskLeo2.m4a",
        }                                                                          

         file { "/Library/Scripts/PeanutButterJellyTime.mp3":
        ensure          => file,
        owner           => "root",
        group           => "admin",
        mode            => 755,
        source          => "$::kcs_files/files/bell/PeanutButterJellyTime.mp3",
        }                                                                          

         file { "/Library/Scripts/SchoolsOutShort.aiff":
        ensure          => file,
        owner           => "root",
        group           => "admin",
        mode            => 755,
        source          => "$::kcs_files/files/bell/SchoolsOutShort.aiff",
        }                                                                          

        file { "/Library/Scripts/bell.aiff":
        ensure          => file,
        owner           => "root",
        group           => "admin",
        mode            => 755,
        source          => "$::kcs_files/files/bell/bell.aiff",
        }                                                                          


        # file { '/Users/kadmin/Documents/StartClock.app' :
        #   ensure  => present,
        #   owner   => 'kadmin',
        #   group   => 'staff',
        #   mode    => '0644',
        #   recurse => true,
        #   source  => '$::kcs_files/files/bell/StartClock.app',
        # }
        # 

  file { '/etc/crontab' :
    ensure  => present,
    owner   => 'root',
    group   => 'wheel',
    mode    => '0644',
  }

  file { '/usr/lib/cron/tabs/kadmin' :
    ensure  => present,
    owner   => 'root',
    group   => 'wheel',
    mode    => '0600',
    source  => "$::kcs_files/files/bell/nwcrontab",
  }

  file { '/Library/LaunchDaemons/org.kentoncityschools.nwbell.plist' :
    ensure  => absent,
    owner   => 'root',
    mode    => '0644',
    source  => "$::kcs_files/files/bell/org.kentoncityschools.nwbell.plist",
    alias   => 'nwbell'
  }

  exec { "systemsetup -settimezone America/New_York":
          onlyif  => ["test `systemsetup -gettimezone | grep \"Time Zone\"| cut -c 12-` != \"America/New_York\""],
  }
  exec { "systemsetup -setusingnetworktime on":
          onlyif  => ["test `systemsetup -getusingnetworktime | grep \"Network Time\" | cut -c 15-16` != \"On\""],
  }
  exec { "systemsetup -setnetworktimeserver ntp1.woco-k12.org":
          onlyif  => ["test `systemsetup -getnetworktimeserver | grep \"Network Time\" | cut -c 22-` != \"ntp1.woco-k12.org\""],
  }

# Set energy saver
        exec { "pmset -a sleep 0":
                # There is a space and a tab between the brackets
                onlyif => ["test `pmset -g | sed -e 's/^[       ]*//' | sed -E 's/[     ]+/,/g' | grep ^sleep | cut -d ',' -f 2` != '0'"]
        }
        exec { "pmset -a displaysleep 10":
                onlyif => ["test `pmset -g | sed -e 's/^[       ]*//' | sed -E 's/[     ]+/,/g' | grep ^displaysleep | cut -d ',' -f 2` != '10'"]
        }

        exec { "pmset -a disksleep 60":
                onlyif => ["test `pmset -g | sed -e 's/^[       ]*//' | sed -E 's/[     ]+/,/g' | grep ^disksleep | cut -d ',' -f 2` != '60'"]
        }

      exec { "pmset repeat cancel;pmset repeat sleep MTWRF 20:00:00 wakeorpoweron MTWRF 7:00:00" :
      #exec { "pmset repeat cancel;pmset repeat wakeorpoweron MTWRF 6:00:00" :
      #         exec { "pmset repeat cancel" :
                    unless => ["pmset -g sched | grep 'wakepoweron at 7:00AM'",
                               "pmset -g sched | grep 'sleep at 8:00PM'"
                              ],
      }
      
}
