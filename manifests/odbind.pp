
class odbind {

case $sp_local_host_name {
		/^es-/ : { $od = 'od.kes.kcs' }
		/^hs-/ : { $od = 'od.khs.kcs' }
		/^ms-/ : { $od = 'od.kms.kcs' }
		/^ol-/ : { $od = 'ol.kcs' }
    /^nw-/ : { $od = 'od.khcs.kcs' }
	}

file { '/var/root/odbind.sh' :
  ensure  => present,
  owner   => 'root',
  mode    => '0700',
  source  => '/etc/puppetlabs/puppet/files/macOS/odbind.sh',
}


exec { "set-odmaster":
  require  => File["/var/root/odbind.sh"], 
  onlyif	=> "test '$odmaster' != '$od' || test `/usr/bin/dscl -q localhost -read /Search CSPSearchPath | grep -c $od` == '0'",
	command => "/var/root/odbind.sh",
  }
}
