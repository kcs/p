class munki {
  # Set Package resource defaults for OS X clients
  Package{ensure => installed,provider => pkgdmg}

  $munki = "munkitools-3.0.3.3352.pkg"

	package{"$munki": 	
		source => "https://munki.thekcs.net/repo/pkgs/utilities/$munki",
	}
 
  exec { "defaults write /Library/Preferences/ManagedInstalls SoftwareRepoURL https://munki.thekcs.net/repo":
    unless => ["test `defaults read /Library/Preferences/ManagedInstalls SoftwareRepoURL` = https://munki.thekcs.net/repo" ],
    require	=> Package[$munki],
  }

  exec { "defaults write /Library/Preferences/ManagedInstalls InstallAppleSoftwareUpdates -bool YES":
    unless => "defaults read /Library/Preferences/ManagedInstalls InstallAppleSoftwareUpdates | grep 1",
    require	=> Package[$munki],
  }

  exec { "defaults write /Library/Preferences/ManagedInstalls SuppressStopButtonOnInstall -bool YES":
    unless => "defaults read /Library/Preferences/ManagedInstalls SuppressStopButtonOnInstall | grep 1",
    require	=> Package[$munki],
    }

  exec { "defaults write /Library/Preferences/ManagedInstalls InstallRequiresLogout -bool false":
    unless => "defaults read /Library/Preferences/ManagedInstalls InstallRequiresLogout | grep 0",
    require	=> Package[$munki],
  }

	service { "com.googlecode.munki.managedsoftwareupdate-manualcheck":
			enable          =>      true,
			ensure          =>      running,
	#                subscribe       =>      File["/Library/LaunchDaemons/com.googlecode.munki.managedsoftwareupdate-manualcheck.plist"],
	#                require         =>      File["/Library/LaunchDaemons/com.googlecode.munki.managedsoftwareupdate-manualcheck.plist"],
			 require         =>      Package[$munki],
		}

		service { "com.googlecode.munki.managedsoftwareupdate-install":
			enable          =>      true,
			ensure          =>      running,
	#                subscribe       =>      File["/Library/LaunchDaemons/com.googlecode.munki.managedsoftwareupdate-install.plist"],
	#                require         =>      File["/Library/LaunchDaemons/com.googlecode.munki.managedsoftwareupdate-install.plist"],
			 require         =>      Package[$munki],
		}

		service { "com.googlecode.munki.managedsoftwareupdate-check":
			enable          =>      true,
			ensure          =>      running,
	#                subscribe       =>      File["/Library/LaunchDaemons/com.googlecode.munki.managedsoftwareupdate-check.plist"],
	#                require         =>      File["/Library/LaunchDaemons/com.googlecode.munki.managedsoftwareupdate-check.plist"],
			 require         =>      Package[$munki],
		}
    
  # SUS uses Apache Rewrites to select the correct catalog based on macOS version
  # For Munki
  exec {'defaults write "/Library/Preferences/ManagedInstalls" "SoftwareUpdateServerURL" "https://sus.thekcs.net/index.sucatalog"':
  unless  => ['test `defaults read /Library/Preferences/ManagedInstalls SoftwareUpdateServerURL` = "https://sus.thekcs.net/index.sucatalog"'],
  require => Package[$munki],
  }

  # For SoftwareUpdate
  exec {'defaults write "/Library/Preferences/com.apple.SoftwareUpdate" "CatalogURL" "https://sus.thekcs.net/index.sucatalog"':
  unless  => ['test `defaults read /Library/Preferences/com.apple.SoftwareUpdate CatalogURL` = "https://sus.thekcs.net/index.sucatalog"'],
  require => Package[$munki],
  }



  exec { "defaults write /Library/Preferences/ManagedInstalls ClientIdentifier site_default":
       unless => ["test `defaults read /Library/Preferences/ManagedInstalls ClientIdentifier` = \"site_default\" " ],
	}

}
